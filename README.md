# Tamaris

Tamaris is a tool that allows you to create comments on merges request based on a json SAST report

## Usage
```commandline
main.py [-h] -f FILE -t TOKEN [-u URL] -p PROJECT_NAME -i ID

options:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  Path to report
  -t TOKEN, --token TOKEN
                        Gitlab API token
  -u URL, --url URL     Url of gitlab instance, default=https://gitlab.com
  -p PROJECT_NAME, --project_name PROJECT_NAME
                        Namespace of project (e.g. https://gitlab.com/NAMESPACE/PROJECT => -p "NAMESPACE/PROJECT")
  -i ID, --id ID        Merge request id
```


## Available variables
- category
- message
- description
- cve
- severity
- scanner.id
- scanner.name
- location.file
- location.start_line
- location.end_line

Example : 
```
category -> sast
message -> Improper Neutralization of Special Elements used in an SQL Command ('SQL Injection')
description -> The method identified is susceptible to injection. The input should be validated and properly
escaped.
cve -> 
severity -> Critical
scanner.id -> semgrep
scanner.name -> Semgrep
location.file -> UserDAO.java
location.start_line -> 45
location.end_line -> 46
```

![Output](./img/output.png)

## Todo

- [ ] handle other templates
- [ ] restructure the code into files
- [ ] handle minimum severity to create comments
- [ ] provide examples of how to use it in a CI

## License
This project is licensed under the [MIT](https://opensource.org/licenses/MIT).