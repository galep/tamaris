FROM python:3.10-alpine
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt && rm -f requirements.txt
CMD ["python", "tamaris.py"]