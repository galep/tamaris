from tamaris import compute_body, compute_position

vulnerability = {
    'id': '5a0aa0eb3e0b04c23116e3ec5af028de79a90360ce61da553334eb21ec07d22c',
    'category': 'sast',
    'message': 'Improper Neutralization of Special Elements used in an SQL Command (\'SQL Injection\')',
    'description': 'The input values included in SQL queries need to be passed in safely. Bind\n' +
                   'variables in prepared statements can be used to easily mitigate the risk of\nSQL injection.\n',
    'cve': 'FAKE_CVE',
    'severity': 'Critical',
    'scanner': {
        'id': 'semgrep',
        'name': 'Semgrep'
    },
    'location': {
        'file': 'BaseDAO.java',
        'start_line': 36,
        'end_line': 42
    },
    'identifiers': [
        {
            'type': 'semgrep_id',
            'name': 'find_sec_bugs.SQL_INJECTION_SPRING_JDBC-1',
            'value': 'find_sec_bugs.SQL_INJECTION_SPRING_JDBC-1'
        },
        {
            'type': 'cwe',
            'name': 'CWE-89',
            'value': '89',
            'url': 'https://cwe.mitre.org/data/definitions/89.html'
        },
        {
            'type': 'find_sec_bugs_type',
            'name': 'Find Security Bugs-SQL_INJECTION_SPRING_JDBC',
            'value': 'SQL_INJECTION_SPRING_JDBC'
        },
        {
            'type': 'find_sec_bugs_type',
            'name': 'Find Security Bugs-SQL_INJECTION_JPA',
            'value': 'SQL_INJECTION_JPA'
        },
        {
            'type': 'find_sec_bugs_type',
            'name': 'Find Security Bugs-SQL_INJECTION_JDO',
            'value': 'SQL_INJECTION_JDO'
        },
        {
            'type': 'find_sec_bugs_type',
            'name': 'Find Security Bugs-SQL_INJECTION_JDBC',
            'value': 'SQL_INJECTION_JDBC'
        },
        {
            'type': 'find_sec_bugs_type',
            'name': 'Find Security Bugs-SQL_NONCONSTANT_STRING_PASSED_TO_EXECUTE',
            'value': 'SQL_NONCONSTANT_STRING_PASSED_TO_EXECUTE'
        }
    ]
}
base_expected_body = 'SAST:Critical\n\n'
base_expected_body += 'Improper Neutralization of Special Elements used in an SQL Command (\'SQL Injection\')\n\n'
base_expected_body += '[CVE](https://cve.mitre.org/cgi-bin/cvename.cgi?name=FAKE_CVE)\n\n'
base_expected_body += '[Common Weakness Enumeration](https://cwe.mitre.org/data/definitions/89.html)\n\n'


def test_compute_body_multiline():
    expected_body = base_expected_body + 'From line 36 to line 42\n\n'
    expected_body += 'The input values included in SQL queries need to be passed in safely. Bind\n'\
        'variables in prepared statements can be used to easily mitigate the risk of\nSQL injection.\n\n\n'
    expected_body += 'Created by [Tamaris](https://gitlab.com/galep/tamaris)'

    body, start_line, end_line = compute_body(vulnerability)
    assert start_line == 36
    assert end_line == 42
    assert body == expected_body


def test_compute_body_oneline():
    vulnerability['location'] = {'file': 'BaseDAO.java', 'start_line': 36}
    expected_body = base_expected_body + 'At line 36\n\n'
    expected_body += 'The input values included in SQL queries need to be passed in safely. Bind\n'\
        'variables in prepared statements can be used to easily mitigate the risk of\nSQL injection.\n\n\n'
    expected_body += 'Created by [Tamaris](https://gitlab.com/galep/tamaris)'

    body, start_line, end_line = compute_body(vulnerability)
    assert start_line == 36
    assert body == expected_body


class MrDiff(object):
    base_commit_sha = ''
    start_commit_sha = ''
    head_commit_sha = ''

    def __init__(self, base, start, head):
        self.base_commit_sha = base
        self.start_commit_sha = start
        self.head_commit_sha = head


def test_compute_position():
    expected_position = {
        'base_sha': 'base_commit_sha',
        'start_sha': 'start_commit_sha',
        'head_sha': 'head_commit_sha',
        'position_type': 'text',
        'old_path': 'BaseDAO.java',
        'new_path': 'BaseDAO.java',
        'new_line': 36,
        'line_range': {
            'start': {
                'line_code': '47b60d5a2a0de5a84d5982c9b52142fe3cc32b8a_36_36',
                'type': 'new'
            },
            'end': {
                'line_code': '47b60d5a2a0de5a84d5982c9b52142fe3cc32b8a_42_42',
                'type': 'new'
            }
        }
    }

    mr_diff = MrDiff('base_commit_sha', 'start_commit_sha', 'head_commit_sha')
    start_line = 36
    end_line = 42
    position = compute_position(mr_diff, 'BaseDAO.java', start_line, end_line)

    assert position == expected_position
