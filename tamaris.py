import gitlab
import json
import argparse
import hashlib
import jinja2

parser = argparse.ArgumentParser(description='')


class Scanner:
    id = None
    name = None


class Location:
    file = None
    start_line = None
    end_line = None


class Cwe:
    type = None
    name = None
    value = None
    url = None


class Variables:
    category = None
    message = None
    description = None
    cve = None
    severity = None
    scanner = Scanner()
    location = Location()
    cwe = Cwe()

    def fill(self, vulnerability):
        self.category = safe_get_from_json(vulnerability, 'category')
        self.message = safe_get_from_json(vulnerability, 'message')
        self.description = safe_get_from_json(vulnerability, 'description')
        self.cve = safe_get_from_json(vulnerability, 'cve')
        self.severity = safe_get_from_json(vulnerability, 'severity')
        self.scanner.id = safe_get_from_json(vulnerability['scanner'], 'id')
        self.scanner.name = safe_get_from_json(vulnerability['scanner'], 'name')
        self.location.file = safe_get_from_json(vulnerability['location'], 'file')
        self.location.start_line = safe_get_from_json(vulnerability['location'], 'start_line')
        self.location.end_line = safe_get_from_json(vulnerability['location'], 'end_line')

def safe_get_from_json(json, keyword, default=None):
    if keyword in json:
        return json[keyword]
    else:
        return default

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader, autoescape=True)
TEMPLATE_FILE = "template.txt.j2"


def init():
    parser.add_argument('-f', '--file', help='Path to report', required=True)
    parser.add_argument('-t', '--token', help='Gitlab API token', required=True)
    parser.add_argument('-u', '--url', help='Url of gitlab instance, default=https://gitlab.com',
                        default='https://gitlab.com')
    parser.add_argument('-p', '--project_name', help='Namespace of project (e.g. https://gitlab.com/NAMESPACE/PROJECT '
                                                     '=> -p "NAMESPACE/PROJECT")', required=True)
    parser.add_argument('-i', '--id', help='Merge request id', required=True)
    parser.add_argument('-d', '--debug', action=argparse.BooleanOptionalAction)
    return parser.parse_args()


def compute_position(mr_diff, file, start_line, end_line):
    hashed_file_name = hashlib.sha1(file.encode('utf-8'))
    line_range_start = str(hashed_file_name.hexdigest())
    line_range_start += '_' + str(start_line) + '_' + str(start_line)
    if end_line is not None:
        line_range_end = str(hashed_file_name.hexdigest())
        line_range_end += '_' + str(end_line) + '_' + str(end_line)
    else:
        line_range_end = str(hashed_file_name.hexdigest())

    print('start line = ', start_line)
    print('end line = ', end_line)
    position = {
        'base_sha': mr_diff.base_commit_sha,
        'start_sha': mr_diff.start_commit_sha,
        'head_sha': mr_diff.head_commit_sha,
        'position_type': 'text',
        'old_path': file,
        'new_path': file,
        'new_line': start_line,
        'line_range': {
            'start': {
                'line_code': line_range_start,
                'type': 'new'
            },
            'end': {
                'line_code': line_range_end,
                'type': 'new'
            }
        }
    }
    return position


def parse_file(args):
    file_path = args.file
    results = []
    with open(file_path) as file:
        content = json.load(file)
        vulnerabilities = content['vulnerabilities']
        for vulnerability in vulnerabilities:
            template = templateEnv.get_template(TEMPLATE_FILE)
            data = Variables()
            data.fill(vulnerability)
            rendered_model = template.render(vars(data))
            results.append({'data': data, 'body': rendered_model})
    return results


if __name__ == '__main__':
    args = init()
    results = parse_file(args)
    gl = gitlab.Gitlab(url=args.url, private_token=args.token)
    gl.auth()
    project = gl.projects.get(args.project_name)
    merge_request = project.mergerequests.get(args.id)
    mr_diff = merge_request.diffs.list()[0]
    for result in results:
        position = compute_position(mr_diff,
                                    result['data'].location.file,
                                    result['data'].location.start_line,
                                    result['data'].location.end_line)
        print(position)
        merge_request.discussions.create({'body': result['body'], 'position': position})
        qwer
