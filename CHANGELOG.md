# CHANGELOG

## [Unreleased]

## [1.0.0]
- Handling of semgrep reports
- Handling of multi-line comments
